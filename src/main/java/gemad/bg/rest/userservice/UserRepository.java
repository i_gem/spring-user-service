package gemad.bg.rest.userservice;


import org.springframework.data.jpa.repository.JpaRepository;

//repository for handling (updating, deleting, creating) user data
public interface UserRepository extends JpaRepository<User, String> {

}
