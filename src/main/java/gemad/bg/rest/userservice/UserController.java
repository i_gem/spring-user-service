package gemad.bg.rest.userservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class UserController {

    @Autowired PasswordEncoder encoder;

    private final UserRepository repository;
    private final UserResourceAssembler assembler;

    UserController(UserRepository repository, UserResourceAssembler assembler){
        this.repository = repository;
        this.assembler = assembler;
    }

    @GetMapping("/users")
    Resources<Resource<User>> all() {
        List<Resource<User>> users = repository.findAll().stream()
                .map(assembler::toResource).collect(Collectors.toList());

        return new Resources<>(users,
                linkTo(methodOn(UserController.class).all()).withSelfRel());
    }

//    @PostMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PostMapping("/users")
    User newUser(@RequestBody User newUser) {
        final String encoded = encoder.encode(newUser.getPassword());
        newUser.setPassword(encoded);
        return repository.save(newUser);
    }

    @GetMapping("/users/{email}")
    Resource<User> one(@PathVariable String email) {

        User user = repository.findById(email)
                .orElseThrow(() -> new UserNotFoundException(email));

        return assembler.toResource(user);
    }

//    @PutMapping(value = "/users/{email}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @PutMapping("/users/{email}")
    User replaceUser(@RequestBody User newUser, @PathVariable String email) {

        return repository.findById(email)
                .map(user -> {
                    user.setFirstName(newUser.getFirstName());
                    user.setLastName(newUser.getLastName());
                    user.setDateOfBirth(newUser.getDateOfBirth());
                    user.setPassword(newUser.getPassword() != null ? encoder.encode(newUser.getPassword()) : null );
                    return repository.save(user);
                })
                .orElseGet(() -> {
                    newUser.setEmail(email);
                    return repository.save(newUser);
                });
    }

    @PatchMapping("/users/{email}")
    public User partialUpdate(@RequestBody User newUser, @PathVariable String email) {
        User user = repository.findById(email)
                .orElseThrow(() -> new UserNotFoundException(email));

        if (newUser.getFirstName() != null)
            user.setFirstName(newUser.getFirstName());
        if (newUser.getLastName() != null)
            user.setLastName(newUser.getLastName());
        if (newUser.getDateOfBirth() != null)
            user.setDateOfBirth(newUser.getDateOfBirth());
        if (newUser.getPassword() != null)
            user.setPassword(encoder.encode(newUser.getPassword()));

        return repository.save(user);
    }

    @DeleteMapping("/users/{email}")
    void deleteUser(@PathVariable String email) {
        repository.deleteById(email);
    }

}
