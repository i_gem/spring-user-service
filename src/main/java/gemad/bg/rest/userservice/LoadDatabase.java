package gemad.bg.rest.userservice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

//filling database with example values
@Configuration
@Slf4j
public class LoadDatabase {
    @Autowired
    PasswordEncoder encoder;

    @Bean
    CommandLineRunner initDatabase(UserRepository repository) {
        return args -> {
            log.info("Preloading " + repository.save(new User("Linus", "Torvalds", "28.12.1969", "notreal@mail.com", encoder.encode("12345"))));
            log.info("Preloading " + repository.save(new User("Richard", "Stallman", "16.03.1953", "not_so_true@gnu.org", encoder.encode("qwerty"))));
        };
    }
}
