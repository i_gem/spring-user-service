package gemad.bg.rest.userservice;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class User {




    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private @Id String email;
    private String password;

    User(String firstName, String lastName, String dateOfBirth, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.email = email;
        this.password = password;
    }


    //Tomcat needs default constructor
    User(){

    }
}
