This program is a simple Spring REST web service. This web service can store user information such as first name, last name, date of birth, email and password. Passwords are stored in hashed form.
Basic HTTP is used for authentication.
The service supports GET, POST, PUT, PATCH and DELETE requests.

Request examples:

curl -X POST -H "Content-type: application/json" -d "{\"firstName\": \"Some\", \"lastName\": \"Man\", \"dateOfBirth\": \"10.10.1910\", \"email\": \"wow@email.com\", \"password\": \"pss\"}" admin:123@localhost:8080/users

curl -X PUT -H "Content-type: application/json" -d "{\"firstName\": \"Some\"}" admin:123@localhost:8080/users/notreal%40mail.com

curl -X DELETE admin:123@localhost:8080/users/wow%40email.com

curl -X GET admin:123@localhost:8080/users/not_so_true%40gnu.org

curl -X PATCH -H "Content-type: application/json" -d "{\"password\": \"easyPass\"}" admin:123@localhost:8080/users/not_so_true%40gnu.org
